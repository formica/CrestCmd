#include <cassert>
#include <iostream>

#include <nlohmann/json.hpp>

#include <CrestApi/CrestApi.h>
#include <filesystem> 

#include <getopt.h>
#include <stdio.h>

#include <boost/lexical_cast.hpp>

using boost::lexical_cast;
using boost::bad_lexical_cast;

using namespace Crest;
using namespace std;

std::string SERVER_PATH = "";        // Path (URL) to CREST Server
std::string LOCAL_PATH = "";         // Path to the local file storage
std::string TAG_NAME = "";           // Tag to create a dump
// std::string DATA_SOURCE = "server";  // Data source where to reade the data
int PAGE_SIZE = 10;                  // number of IOVs/payloads to download/upload together
long SINCE = 0;
long UNTIL = -1;
int FLAG = 0;                        // Parameter indicates that SINCE or UNTIL were changed

void printParams(){
  std::cout << std::endl;
  std::cout << "PARAMETER LIST:" << std::endl;
  std::cout << "server path = " << SERVER_PATH << std::endl;
  std::cout << "local path  = " << LOCAL_PATH << std::endl;
  std::cout << "tag name    = " << TAG_NAME << std::endl;
  std::cout << "page size = " << PAGE_SIZE << std::endl;
  std::cout << "since = " << SINCE << std::endl;
  std::cout << "until = " << UNTIL << std::endl;
  std::cout << std::endl;
}

std::string getIOVHash(nlohmann::json js){
  std::string response = js.value("payloadHash", "");
  if (response == ""){
    throw std::runtime_error("ERROR: IOV has no \"hash\" field");
  }
  return response;
}

std::string getIOVSince(nlohmann::json js){
  std::string response = js["since"].dump();
  return response;
}

int getIOVSince2(nlohmann::json js){
  int response = js.value("since", -1);
  if (response == -1){
    throw std::runtime_error("ERROR: IOV has no \"since\" field");
  }
  return response;
}

int getParams(int argc, char * argv[]){

  // std::cout << std::endl;
  
  // parameter printing:
  // for (int i = 0; i < argc; i++){
  //   std::cout << "arg[" << i << "] = " << argv[i] << std::endl;
  // }
  // std::cout << std::endl;

   int c;
   std::string tagname = "";
   std::string fromParam = "server"; // server or fileSystem

   const char    * short_opt = "hn:l:s:p:i:u:";

// new (example):
// crestDump -from datasource -localPath localpath -server serverpath -tag tagname

   struct option   long_opt[] =
   {
      {"help",             no_argument,       NULL, 'h'},
      {"tag",              required_argument, NULL, 'n'},
      {"localPath",        required_argument, NULL, 'l'},
      {"server",           required_argument, NULL, 's'},
      {"pageSize",         required_argument, NULL, 'p'},
      {"since",            required_argument, NULL, 'i'},
      {"until",            required_argument, NULL, 'u'},    
      {NULL,               0,                 NULL, 0  }
   };

   while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
   {
      switch(c)
      {
         case -1:       /* no more arguments */
         case 0:        /* long options toggles */
         break;

         case 'n':
	 // printf("n: you entered \"%s\"\n", optarg);
         TAG_NAME = optarg;
         break;

         case 'l':
	 // printf("l: you entered \"%s\"\n", optarg);
         LOCAL_PATH = optarg;
         break;

         case 's':
	 // printf("s: you entered \"%s\"\n", optarg);
         SERVER_PATH = optarg;
         break;

         case 'p':
	 // printf("s: you entered \"%s\"\n", optarg);
         try {
           PAGE_SIZE = std::stoi(optarg);
	 }
         catch (...){
	   std::cout << "ERROR: wrong page size (IOV number) parameter, cannot convert " << optarg << " to int." << std::endl;
           exit(0);
         }
         break;

         case 'i':
         try {
           SINCE = std::stoi(optarg);
           FLAG = 1;
	 }
         catch (...){
	   std::cout << "ERROR: wrong since parameter, cannot convert " << optarg << " to long." << std::endl;
           exit(0);
         }
         break;

         case 'u':
	 // printf("s: you entered \"%s\"\n", optarg);
         try {
           UNTIL = std::stoi(optarg);
           FLAG = 1;
	 }
         catch (...){
	   std::cout << "ERROR: wrong until parameter, cannot convert " << optarg << " to long." << std::endl;
           exit(0);
         }
         break;

         case 'h':
         printf("Usage: crestImport [OPTIONS]\n");
         printf("  -n, --tag         tag name, required parameter;\n");
         printf("  -l, --localPath   local file storage directory path, optional;\n");
         printf("  -s, --serverPath  server path, required parameter;\n");
         printf("  -p, --pageSize    number of IOVs to download together (page size),\n");
         printf("                    optional parameter;\n");
         printf("  -i, --since   since time to set the time interval, optional;\n");
         printf("  -u, --until   until time to set the time interval, optional;\n");
         printf("  -h, --help        print this help and exit.\n");
         printf("\n");

         exit(0);


         case ':':
         case '?':
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);

         default:
         fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
         fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
         return(-2);
      };
   };

   // Orphaned parameters
   //
  if (optind < argc)
    {
      printf ("ERROR: non-option ARGV-elements: ");
      while (optind < argc) {
        printf ("%s ", argv[optind++]);
      }
      putchar ('\n');
      exit(0);
  }
   //

   // std::cout << "  tagName = " << tagname << std::endl;
   if (TAG_NAME == ""){
     std::cerr << "ERROR: please enter tag name!" << std::endl;
     exit(0);
   }


   if (SERVER_PATH == ""){
     std::cerr << "ERROR: please enter the server path!" << std::endl;
     exit(0);
   }

   return(0);
}

int main(int argc, char* argv[]) {

  int retv = 0;

  getParams(argc-0, &argv[0]);
  // printParams();

  std::cout << "Tag data uploading..." << std::endl;

  CrestClient* crestSource; 
  CrestClient* crestTarget;

  nlohmann::json tag; 

  if (LOCAL_PATH == ""){
    crestSource = new CrestClient(true);
  }
  else {
    crestSource = new CrestClient(true,LOCAL_PATH);
  }
  crestTarget = new CrestClient(SERVER_PATH);


  // TAG

  try{ 
    tag = crestSource->findTag(TAG_NAME);
    
    try{
      crestTarget->createTag(tag[0]);
      std::cout << "tag " << TAG_NAME << " was written on CREST server." << std::endl; 
    }
    catch (const std::exception& e) {
      std::cout << std::endl << "Cannot write tag:" << std::endl;
      std::cout << e.what() << std::endl;
    }
    

  }
  catch (const std::exception& e) {
    std::cout << std::endl << "Cannot get tag:" << std::endl;
    std::cout << e.what() << std::endl;
    exit(0);
  }

  // TAG META INFO

  try{ 
    nlohmann::json tagMetaInfo = crestSource->getTagMetaInfo(TAG_NAME);
    
    try{
      crestTarget->createTagMetaInfo(tagMetaInfo[0]);
      std::cout << "tag meta info data are uploaded on CREST server." << std::endl;
    }
    catch (const std::exception& e) {
      std::cout << std::endl << "Cannot write tag meta info for tag with name \"" << TAG_NAME << "\"" << std::endl;
      std::cout << e.what() << std::endl;
    }
    

  }
  catch (const std::exception& e) {
    std::cout << std::endl << "Cannot get tag meta info for tag with name \"" << TAG_NAME << "\"" << std::endl;
    std::cout << e.what() << std::endl;
    // exit(0);
  }


  // This code fragment works when one or more time interval limits are set (i.e. since-until).
  // It uses selectIovs(TAG_NAME, SINCE, UNTIL) method.
  if (FLAG == 1){
    // std::cout << "ALGORITHM 2 (selectIovs):" << std::endl;
    std::cout << "since interval = [ " << SINCE << " ; " << UNTIL << " ]" << std::endl;

    nlohmann::json iovList = nlohmann::json::array();

    try{ 
      iovList = crestSource->selectIovsFS(TAG_NAME, SINCE, UNTIL);

      // IOV list printing:
      // std::cout << std::endl << " IOV list for TAG \"" << TAG_NAME << "\" = " 
      //           << std::endl << iovList.dump(4) << std::endl;
    }
    catch (const std::exception& e) {
      std::cout << std::endl << "Cannot get IOV list for tag with name \"" << TAG_NAME << "\"" << std::endl;
      std::cout << e.what() << std::endl;
      exit(0);
    }

    int iovSize = 0;

    try{ 
      iovSize = iovList.size();
    }
    catch (const std::exception& e) {
      std::cout << std::endl << "Cannot get IOV list for tag with name \"" << TAG_NAME << "\"" << std::endl;
      std::cout << e.what() << std::endl;
      iovSize = 0;

    }

    std::cout << "IOV number = " << iovSize << std::endl;


    int stepNumber = iovSize / PAGE_SIZE;
    int stepsA = iovSize % PAGE_SIZE;

    // std::cout << "steps (a) = " << stepNumber << std::endl; 
    // std::cout << "stepsA    = " << stepsA << std::endl; 
    if (stepsA != 0) stepNumber = stepNumber + 1;
    // std::cout << "steps (a) = " << stepNumber << std::endl; 

    for (int i = 0; i < stepNumber; i++){
      int startN = PAGE_SIZE * i;
      int lastN = PAGE_SIZE * (i + 1);
      if (i == stepNumber - 1) lastN = iovSize;
      // std::cout << "i = " << i << "    startN = " << startN <<  "    lastN = " << lastN << std::endl;
      nlohmann::json iovPayloads = nlohmann::json::array();  // to store data on CREST Server, it in not used with server -> file dump.
      for (int j = startN; j < lastN; j++){
	// std::cout << "      j = " << j << std::endl;

        nlohmann::json iov = iovList[j];
	std::string hash = "";
        try{ 
          hash = getIOVHash(iov);
          // std::cout << std::endl << "hash(" << j << ") = " << hash << std::endl;
        }
        catch (const std::exception& e) {
          std::cout << "ERROR: Cannot get a hash:" << std::endl;
          std::cout << "  tag name = " << TAG_NAME << std::endl;
          std::cerr << e.what() << std::endl;
          exit(0);
        }
	
        std::string payload = "";
        try{
          payload = crestSource->getPayloadAsString(hash);

          // std::cout << "payload (\"" << hash << "\") =" << std::endl;
          // std::cout << payload << std::endl;

        }
        catch (const std::exception& e) {
          std::cout << "ERROR: Cannot get a payload:" << std::endl;
          std::cout << "  tag name = " << TAG_NAME << std::endl;
          std::cout << "  hash = " << hash << std::endl;
          std::cerr << e.what() << std::endl;
        }

        std::string since = "";   

        try{ 
         since = getIOVSince(iov);
         // std::cout << std::endl << "since(" << j << ") = " << since << std::endl;
        }
        catch (const std::exception& e) {
          std::cout << "ERROR: Cannot get \"since\":" << std::endl;
          std::cout << "  tag name = " << TAG_NAME << std::endl;
          std::cout << e.what() << std::endl;
          exit(0);
        }

	//
        try{
          nlohmann::json item =
          {
            {"payloadHash", payload},
            // {"tagName", TAG_NAME},
            {"since", since}
          };

          iovPayloads.push_back(item);

        }
        catch (const std::exception& e) {
          std::cout << "ERROR: Cannot save data (tag,since,payload) in JSON array to store it later:" << std::endl;
          std::cout << "  tag name = " << TAG_NAME << std::endl;
          std::cout << "  hash = " << hash << std::endl;
          std::cout << "  since = " << since << std::endl;
          std::cerr << e.what() << std::endl;
        }

      } // j

      ///

      // printing for iovPayloads:    
      // std::cout << "IOVs and Payloads for tag \"" << TAG_NAME << "\" :" << std::endl;
      // std::cout << iovPayloads.dump(4) << std::endl;
      uint64_t endtime = 0;
      try{
        crestTarget->storeBatchPayloads(TAG_NAME, endtime, iovPayloads); // to make try/catch
      }
      catch (const std::exception& e) {
        std::cout << "ERROR: Cannot save the data (iovs and payloads) on CREST server for the tag \"" 
	          << TAG_NAME << "\"."<< std::endl;
        std::cerr << e.what() << std::endl;
      }

      ///

    }

    std::cout << "IOVs and payloads are uploaded on CREST server." << std::endl;
    exit(0);
  }



  // This code fragment works when the time interval is not set (i.e. since-until).
  // It uses findAllIovs method.

  // IOVs

  nlohmann::json iovList;

  // IOV number:

  int iovSize = 0;
  try{ 
    iovSize = crestSource->getSize(TAG_NAME);
    std::cout << "IOV Number for TAG \"" << TAG_NAME << "\" = " << iovSize << std::endl;
  }
  catch (const std::exception& e) {
    std::cout << std::endl << "Cannot get IOV number for tag with name \"" << TAG_NAME << "\"" << std::endl;
    std::cout << e.what() << std::endl;
    exit(0);
  }

  int stepNumber = iovSize / PAGE_SIZE;
  int stepsA = iovSize % PAGE_SIZE;


  // std::cout << "steps (a) = " << stepNumber << std::endl; 
  // std::cout << "stepsA    = " << stepsA << std::endl; 
  if (stepsA != 0) stepNumber = stepNumber + 1;
  // std::cout << "steps (a) = " << stepNumber << std::endl; 

  // std::cout << "TEST 01" << std::endl;

  // the data recieved by pages:
  for (int i = 0; i < stepNumber; i++){
    nlohmann::json iovList = nlohmann::json::array();
    try{
      // std::cout << "TEST 02" << std::endl;
      iovList =  crestSource->findAllIovsFs(TAG_NAME,PAGE_SIZE,i); // OLD
      // std::cout << "IOV List = " << std::endl << iovList.dump(4) << std::endl;
    } // findAllIovsFs
    catch (const std::exception& e) {
      std::cout << "ERROR: Cannot get an IOV list for the tag \"" << TAG_NAME << "\"" << std::endl;
      std::cerr << e.what() << std::endl;
      exit(0);
   } // findAllIovsFs
 
    // std::cout << "TEST 03" << std::endl;

    // 
    int listSize = iovList.size();

    // std::cout << "IOV List length = " << listSize << std::endl;

    nlohmann::json iovPayloads = nlohmann::json::array();  // to store data on CREST Server, it in not used with server -> file dump.


    for (int i = 0; i < listSize; i++) {
      // std::cout << i << std::endl;
      nlohmann::json iov = iovList[i];

      // std::cout << std::endl << " IOV (" << i << ") =" << std::endl;
      // std::cout << iov.dump(4) << std::endl;

      std::string hash = "";
      

      try{ 
        hash = getIOVHash(iov);
        // std::cout << std::endl << "hash(" << i << ") = " << hash << std::endl;
      }
      catch (const std::exception& e) {
        std::cout << "ERROR: Cannot get a hash:" << std::endl;
        std::cout << "  tag name = " << TAG_NAME << std::endl;
        std::cout << e.what() << std::endl;
        exit(0);
      }

      std::string since = "";   

      try{ 
      since = getIOVSince(iov);
      // std::cout << std::endl << "since(" << i << ") = " << since << std::endl;
      }
      catch (const std::exception& e) {
        std::cout << "ERROR: Cannot get \"since\":" << std::endl;
        std::cout << "  tag name = " << TAG_NAME << std::endl;
        std::cerr << e.what() << std::endl;
        exit(0);
      }

      //
      std::string payload = "";
      try{
        payload = crestSource->getPayloadAsString(hash);

        // std::cout << "payload (\"" << hash << "\") =" << std::endl;
        // std::cout << payload << std::endl;

      }
      catch (const std::exception& e) {
        std::cout << "ERROR: Cannot get a payload:" << std::endl;
        std::cout << "  tag name = " << TAG_NAME << std::endl;
        std::cout << "  hash = " << hash << std::endl;
        std::cerr << e.what() << std::endl;
      }
      //

      //
      try{

        nlohmann::json item =
        {
          {"payloadHash", payload},
          // {"tagName", TAG_NAME},
          {"since", since}
        };
        iovPayloads.push_back(item);
      }
      catch (const std::exception& e) {
        std::cout << "ERROR: Cannot save data (tag,since,payload) in JSON array to store it later:" << std::endl;
        std::cout << "  tag name = " << TAG_NAME << std::endl;
        std::cout << "  hash = " << hash << std::endl;
        std::cout << "  since = " << since << std::endl;
        std::cerr << e.what() << std::endl;
      }
      //

    }



    // upload iovPayloads:    
    // std::cout << "IOVs and Payloads for tag \"" << TAG_NAME << "\" :" << std::endl;
    // std::cout << iovPayloads.dump(4) << std::endl;
    uint64_t endtime = 0;
    try{
      crestTarget->storeBatchPayloads(TAG_NAME, endtime, iovPayloads); // to make try/catch
    }
    catch (const std::exception& e) {
      std::cout << "ERROR: Cannot save the data (iovs and payloads) on CREST server for the tag \"" 
	        << TAG_NAME << "\"."<< std::endl;
      std::cerr << e.what() << std::endl;
    }

    //  

  } // i


  std::cout << "IOVs and payloads are uploaded on CREST server." << std::endl;

  return retv;
}
