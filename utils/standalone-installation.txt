CrestCmd and CrestApi standalone installation instruction

This directory contains the scripts and CMakeLists.txt files
to compile the CrestApi libray and the CREST Command line
client (CrestCmd package) without the Athena release.

--------------------------

Software Requirements:

Here is a software list used by me to compile the packages
on my VM.

OS - CentOS Linux 7
gcc - 8.3
cmake - 3.6 or higher  (here was used version 3.16)
(CrestApi requires std::filesystem library)

NLohmann JSON library - included in the CrestApi library  (3.9.1).
Boost - 1.72
CURL library (the version from the CentOS Linux distribution was used)

--------------------------

1) Create a directory, where you wish to compile CrestApi and CrestCmd
packages and enter to this directory:

mkdir <my_project>
cd <my_project>  

2) Clone CrestApi package:

> git clone https://gitlab.cern.ch/mmineev/CrestApi.git

3) Clone CrestCmd package:

> git clone https://gitlab.cern.ch/mmineev/CrestCmd.git

4) Create a "build" directory:

> mkdir ./build

---------------------

5) The standalone mode requires its own CMakeLists.txt files.
They are in the ./CrestCmd/utils directory. Copy them in the main 
project directory and in the CrestApi and CrestCmd directories:

> cp ./CrestCmd/utils/CMakeLists.main ./CMakeLists.txt 
> cp ./CrestCmd/utils/CMakeLists.CrestApi ./CrestApi/CMakeLists.txt
> cp ./CrestCmd/utils/CMakeLists.CrestCmd ./CrestCmd/CMakeLists.txt 

----------------------

6) Set the BOOST_LIB_DIR_PATH to the directory, where was Boost installed: 

> export BOOST_LIB_DIR_PATH=/usr/boost

----------------------

7) Go to the ./build directory:

> cd ./build

----------------------

8) Run cmake:

> cmake -Wno-dev ..

The executables and header files will be installed in the directory
./installed in the main project directory.
(If you would like to change this installation directory, run the
cmake command with an option: 

cmake -DCMAKE_INSTALL_PREFIX=<installation directory> -Wno-dev ..

The "installation directory" is a full path to the chosen directory.

Example:
cmake -DCMAKE_INSTALL_PREFIX=/afs/cern.ch/work/m/mmineev/2021/standalone2/ -Wno-dev ..)


9) Compillation. Run the commands:

> make 
> make install

----------------------

10) Add paths to the installation directory to the PATH and LD_LIBRARY_PATH.
If you used default installation directory (i.e. cmake without CMAKE_INSTALL_PREFIX),
you can use the setup script from ./build directory 

> source ../CrestCmd/utils/setup-exec.sh

(Be carrefull, it uses the relative paths.)

----------------------

11) To use the CREST command line client you have to setup a CREST_SERVER_PATH
variable (The CREST server URL and port number): 

export CREST_SERVER_PATH=http://crest-01.cern.ch:8080

(For crest-01.cern.ch you can use a script: 

> source ../CrestCmd/scripts/crest-setup.sh 

)

---------------------

12) To check if compillation is successfull you can run now the crestCmd

> crestCmd get commands

(This command returns a list of all realised CREST command line client commands.)

